/**
 * 包含应用中所有请求函数的模块
 * 分别暴露
 * 每个函数的返回都是promise对象
 * 
 * 能根据接口文档，定义接口请求
 */

import ajax from './ajax'
import jsonp from 'jsonp'
import { message } from 'antd'

// 处理跨域 默认url
// const BASE = 'http://localhost:5000'，这是后台服务器地址
const BASE = ''

// 登录
export const reqLogin = (username, password) => ajax('/login', { username, password }, 'POST')

// 添加用户
export const reqAddUser = user => ajax('/manage/user/add', user, 'POST')

// 获取一级分类/二级分类列表
// get方法在ajax文件里面已经默认写了
export const reqCategorys = (parentId) => ajax(BASE + '/manage/category/list', { parentId })


// 添加一级分类/二级分类列表
export const reqAddCategorys = (parentId, categoryName) => ajax(BASE + '/manage/category/add', { parentId, categoryName }, 'POST')

// 更新一级分类/二级分类列表
export const reqUpdateCategorys = ({ categoryId, categoryName }) => ajax(BASE + '/manage/category/update', { categoryId, categoryName }, 'POST')


/**
 * 天气
 * 
 */
// jsonp
// jsonp只能处理解决get类型的ajax请求的跨域问题
// jsonp请求不是ajax请求 只是一般的get请求
// 基本原理：
// 浏览器端，动态生成script来请求后台接口  src就是接口的url 定义好用于接收响应数据的函数，并将函数名通过请求参数传递给后台
// 服务器端 接收到处理产生的结果数据后，返回一个函数调用的 js代码，并将结果数据作为实参传入函数调用

// 浏览器端 收到响应自动执行函数调用的 js代码，执行了提前定义好的回调函数，并且取得了需要的结果数据
export const reqWeather = (cityCode) => {

  return new Promise((resolve, reject) => {
    const key = '	ceeb3c23af4230616f4cbdd7a82ee042'
    const url = `https://restapi.amap.com/v3/weather/weatherInfo?city=${cityCode}&key=${key}`
    jsonp(url, {}, (error, data) => {
      // console.log('jsonp', data);
      if (!error && data.status === '1') {
        // 获取天气数据成功
        const { city, province, weather, temperature } = data.lives[0]
        resolve({ city, province, weather, temperature })
      }
      else {
        message.error('获取天气信息失败')
      }
    })
  })

}

/**
 * 获取商品分页列表数据
 */
export const reqProducts = (pageNum, pageSize) => ajax(BASE + '/manage/product/list', { pageNum, pageSize })


/*
搜索商品分页列表 (根据商品名称/商品描述)
searchType: 搜索的类型, productName/productDesc
 */
export const reqSearchProducts = ({pageNum, pageSize, searchName, searchType}) => ajax(BASE + '/manage/product/search', {
  pageNum,
  pageSize,
  [searchType]: searchName,
})
