/**
 * 二次封装发送请求的axios函数
 * 函数的返回值是promis对象
 * 
 * 优化
 * 统一处理请求异常
 */


import axios from 'axios'
import { message } from 'antd'
export default function ajax(url, data = {}, method = 'GET') {

    return new Promise((resolve, reject) => {
        // 1、执行异步ajax请求
        let promise
        if (method === 'GET') {
            // 发送GET请求
            promise = axios.get(url, {
                // 传入配置对象
                params: data,
            })
        }
        else {
            // 发送POST请求
            promise = axios.post(url, data)
        }
        // 2、成功了调用resolve（value）
        promise.then(response => {
            resolve(response)
        }).catch(error => {// 3、失败了不调用reject(reason)  提示异常信息
            message.error('请求出错了！')
        })
    })
}