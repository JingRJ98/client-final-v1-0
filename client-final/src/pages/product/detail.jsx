import React from 'react'
import {
  Card,
  List,
} from 'antd';
import { LeftOutlined } from '@ant-design/icons'
import './style.css'

export default function Detail() {
  const title = (
    <span>
      <LeftOutlined />
      <span>商品详情</span>
    </span>
  )
  return (
    <div>
      <Card title={title}>
        <List>
          <List.Item>
            <span className="left">商品名称:</span>
            <span className="right">{1}</span>
          </List.Item>
          <List.Item>
            <span className="left">商品描述:</span>
            <span className="right">{1}</span>
          </List.Item>
          <List.Item>
            <span className="left">商品价格:</span>
            <span className="right">{1}元</span>
          </List.Item>
          <List.Item>
            <span className="left">所属分类:</span>
            <span className="right">{1} {1 ? ' --> ' + 1 : ''}</span>
          </List.Item>
          <List.Item>
            <span className="left">商品图片:</span>
            <span>
              {/* {
                imgs.map(img => (
                  <img
                    key={img}
                    src={BASE_IMG_URL + img}
                    className="product-img"
                    alt="img"
                  />
                ))
              } */}
            </span>
          </List.Item>
          <List.Item>
            {/* <span className="left">商品详情:</span>
            <span dangerouslySetInnerHTML={{ __html: detail }}>
            </span> */}
          </List.Item>
        </List>
      </Card>
    </div>
  )
}
