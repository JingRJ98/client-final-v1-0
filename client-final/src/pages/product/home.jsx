/**
 * 
 * @returns product添加修改子路由组件
 */
import React, { useState, useEffect } from 'react'
import {
  Card,
  Select,
  Input,
  Button,
  Table,
  Alert
} from 'antd'
import { PlusOutlined } from '@ant-design/icons';
import { reqProducts, reqSearchProducts } from '../../api/index';
import { PAGE_SIZE } from '../../utils/constants'
import LinkButton from '../../components/linkButton'
const { Option } = Select
export default function ProductHome() {
  const [products, setproducts] = useState([])
  const [total, settotal] = useState(0) // 商品总数量
  const [columns, setcolumns] = useState([])
  const [loading, setloading] = useState(false)
  const [searchName, setsearchName] = useState('')
  const [searchType, setsearchType] = useState('productName')
  const [isShowAlert, setisShowAlert] = useState(false)
  const getPages = async (pageNum) => {
    setloading(true)
    let res
    if (setsearchName) {
      res = await reqSearchProducts({ pageNum, pageSize: PAGE_SIZE, searchName, searchType })
    } else {
      res = await reqProducts(pageNum, PAGE_SIZE)
    }
    setloading(false)
    const response = res.data
    if (response.status === 0) {
      const { total, list } = response.data
      settotal(total)
      setproducts(list)
    }
  }
  useEffect(() => {
    getPages(1)
    // eslint-disable-next-line 
  }, [])
  const title = (
    <span>
      <Select
        value={searchType}
        onChange={v => setsearchType(v)}
      >
        <Option value={'productName'}>按名称搜索</Option>
        <Option value={'productDesc'}>按描述搜索</Option>
      </Select>
      <Input
        placeholder={'请输入关键字'}
        style={{ width: '200px', margin: '0 20px' }}
        onChange={e => setsearchName(e.target.value)}
      ></Input>
      <Button
        type={'primary'}
        onClick={() => { }}
      >搜索</Button>
    </span >
  )

  const extra = (
    <Button type='primary'>
      <PlusOutlined />
      添加商品
    </Button>
  )
  // 初始化列
  useEffect(() => {
    setcolumns([
      {
        title: '商品名称',
        dataIndex: 'name',
      },
      {
        title: '商品描述',
        dataIndex: 'desc',
      },
      {
        title: '商品价格',
        dataIndex: 'price',
        render: (price) => `￥${price}`
      },
      {
        width: 100,
        title: '状态',
        dataIndex: 'status',
        render: (status) => {
          return (
            <span>
              <Button type='primary'>下架</Button>
              <span>在售</span>
            </span>
          )
        }
      },
      {
        width: 80,
        title: '操作',
        // dataIndex: 'price',
        render: (product) => {
          return (
            <span>
              <LinkButton onClick={() => {setisShowAlert(true)}}>详情</LinkButton>
              <LinkButton onClick={() => {setisShowAlert(true)}}>修改</LinkButton>
            </span>
          )
        }
      },
    ])
  }, [])

  return (
    <Card title={title} extra={extra}>
      <Table
        loading={loading}
        bordered={true}
        dataSource={products}
        columns={columns}
        // rowKey会自动到dataSource中找到对应的属性名作为 每行的key值
        // 这里不是字符串，是对应的变量
        rowKey="_id"
        pagination={{ defaultPageSize: PAGE_SIZE, total, onChange: getPages }}
      />
      {
        isShowAlert ? (
          <Alert
            message="抱歉"
            description="暂时未开放操作功能"
            type="info"
            showIcon
            closable
          />
        ) : null
      }
    </Card>
  )
}
