import ProductHome from './home'
import ProductAddUpdate from './addupdate'
import Detail from './detail'
import { Route, Switch, NavLink, Link,Redirect } from 'react-router-dom'

/**
 * 
 * @returns 商品
 */
export default function Product() {
  return (
    <Switch>
      {/* exact表示路径完全匹配（严格匹配） */}
      <Route path='/product' component={ProductHome} exact></Route>
      <Route path='/product/addupdate' component={ProductAddUpdate}></Route>
      <Route path='/product/detail' component={Detail}></Route>
      {/* 如果上述的都没有匹配到 则重定向 */}
      <Redirect to='/product'></Redirect>
    </Switch>
  )
}
