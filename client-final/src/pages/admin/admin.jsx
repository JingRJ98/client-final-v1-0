import React, { Component } from 'react';
import userInfo from '../../utils/memoryUtils'
import { Redirect, Route, Switch } from 'react-router-dom'
import { Layout } from 'antd';
import Header from '../../components/header'
import LeftNav from '../../components/leftNav'
import Home from '../home/home'
import Category from '../category/category'
import Product from '../product/product'
import Role from '../role/role'
import User from '../user/user'
import Bar from '../charts/bar'
import Line from '../charts/line'
import Pie from '../charts/pie'


/**
 * 管理路由组件
 */
export default class Admin extends Component {
    render() {
        const { Footer, Sider, Content } = Layout;
        const user = userInfo.user
        if (!user || !user._id) {
            // 如果没有user信息，说明没登录
            // 跳转到登录
            return <Redirect to='/login' />
        }
        return (
            <Layout style={{ height: '100%' }}>
                <Sider>
                    <LeftNav>
                        左侧
                    </LeftNav>
                </Sider>
                <Layout>
                    <Header>Header</Header>
                    <Content style={{ margin:'20px 20px -10px', backgroundColor: '#fff' }}>
                        {/* 分类路由页面内容 */}
                        <Switch>
                            <Route path='/home' component={Home}></Route>
                            <Route path='/category' component={Category}></Route>
                            <Route path='/product' component={Product}></Route>
                            <Route path='/user' component={User} />
                            <Route path='/role' component={Role} />
                            <Route path="/charts/bar" component={Bar} />
                            <Route path="/charts/pie" component={Pie} />
                            <Route path="/charts/line" component={Line} />
                            {/* 如果前面都没有匹配上，跳转到Redirect */}
                            {/* 一开始默认为空，所以也是默认跳转到home首页子路由 */}
                            <Redirect to='/home'></Redirect>
                        </Switch>
                    </Content>
                    <Footer style={{
                        textAlign: 'center',
                        color: '#abb'
                    }}>@Iscca-Jeeen 2021/10/12</Footer>
                </Layout>
            </Layout>
        )
    }
}