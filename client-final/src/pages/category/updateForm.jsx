import React, { Component } from 'react'
// import PropTypes from 'prop-types'
import {
  Form,
  Select,
  Input,

} from 'antd'

const Item = Form.Item
const Option = Select.Option
export default class UpdateForm extends Component {
  // onValuesChange = (changedValues) => {
  //   console.log(changedValues);
  // }
  render() {
    // initCategoryName={myCategory.name}
    const { initCategoryName, getNewCategoryName } = this.props
    return (
      <Form
        // onFinish={this.onFinish}
        // onFinish={(values) => {getNewCategoryName(values.categoryName)}}
        // onFinish必须输入完失去一下焦点才能调用
        // onVC只要有更新就调用
        onValuesChange={(changeValues) => {getNewCategoryName(changeValues.categoryName)}}
      >
        <Item name='categoryName' initialValue=''>
          <Input placeholder={initCategoryName} />
        </Item>
      </Form>
    )
  }
}