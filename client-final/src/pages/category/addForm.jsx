import React, { Component } from 'react'
import {
  Form,
  Select,
  Input,

} from 'antd'

const Item = Form.Item
const Option = Select.Option
export default class AddForm extends Component {
  render() {
    const {
      categorys,
      parentId,
      getNewCategoryName,
      getNewSelect,
    } = this.props
    return (
      <Form>
        <Item name='parentId' initialValue={parentId}>
          <Select onChange={v => getNewSelect(v)}>
            <Option value='0'>一级分类</Option>
            {
              categorys.map(c => {
                return (
                  <Option value={c._id} key={c._id}>{c.name}</Option>
                )
              })
            }
          </Select>
        </Item>
        <Item
          name='categoryName'
          initialValue=''
          onChange={e => getNewCategoryName(e.target.value)}
        >
          <Input placeholder='请输入分类名称' />
        </Item>
      </Form>
    )
  }
}