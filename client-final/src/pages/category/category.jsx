import React, { Component } from 'react';
import {
  Card,
  Table,
  Button,
  message,
  Modal,
} from 'antd'
import { PlusOutlined, ArrowRightOutlined } from '@ant-design/icons'
import LinkButton from '../../components/linkButton'
import './category.css'
import {
  reqCategorys,
  reqAddCategorys,
  reqUpdateCategorys
} from '../../api'
import AddForm from './addForm'
import UpdateForm from './updateForm'

/**
 * 商品分类路由
 */
export default class Category extends Component {
  state = {
    // 一级分类列表
    categorys: [],
    // 子分类列表
    subCategorys: [],
    isLoading: false,
    // 当前需要显示的分类列表的parentId
    parentId: '0',
    // 当前需要显示的分类列表的父分类名称
    parentName: '',
    // 更新或者添加的对话框是否显示 0 都不显示  1 添加  2 更新
    isModalVisible: 0,
  }

  // 初始化table列参数
  initColumns = () => {
    this.columns = [
      {
        title: '分类名称',
        dataIndex: 'name',
        // key: 'name',
      },
      {
        title: '操作',
        width: 340,
        // dataIndex: 'age',
        // key: 'age',
        render: (category) => {
          return (
            <div className='linkbutton-container'>

              <div className='linkbutton-left'>
                <LinkButton onClick={() => { this.showUpdate(category) }}>修改分类</LinkButton>
              </div>

              {this.state.parentId === '0'
                ? <div className='linkbutton-right' onClick={() => this.showSubCategorys(category)}><LinkButton>查看子分类</LinkButton></div>
                : null
              }

            </div>
          )

        },
      },
    ];
  }

  // 获取分类列表并显示
  getCategorys = async () => {
    this.setState({ isLoading: true })
    const { parentId } = this.state
    const res = await reqCategorys(parentId)
    this.setState({ isLoading: false })
    // console.log(res.data);
    if (res.data.status === 0) {
      const categorys = res.data.data
      if (parentId === '0') {
        // 更新一级分类状态
        this.setState({ categorys })
      }
      else {
        // 更新二级分类列表
        this.setState({ subCategorys: categorys })
      }
    }
    else {
      message.error('获取分类列表数据失败')
    }
  }

  // 显示指定一级列表中某个元素的子列表
  showSubCategorys = (item) => {
    this.setState({
      parentId: item._id,
      parentName: item.name
    }, () => {
      // 在状态更新且页面重新渲染之后再执行
      // const { parentId } = this.state
      // console.log('jjjj', parentId);
      this.getCategorys()
    })

  }


  // 返回父列表
  showParent = () => {
    this.setState({
      parentId: '0',
      parentName: '',
      subCategorys: [],
    })
  }


  // 隐藏添加列表和更新列表的对话框
  handleCancel = () => {
    this.setState({
      isModalVisible: 0,
    })
  }

  // 显示添加对话框
  showAdd = () => {
    this.setState({
      isModalVisible: 1
    })
  }
  // 添加商品分类
  addCategory = async () => {
    console.log('添加列表');

    this.setState({
      isModalVisible: 0
    })

    // 收集数据并提交请求
    const name = this.newCategoryName
    const id = this.newSelect
    console.log('数据库已更新', id, name);
    const res = await reqAddCategorys(id, name)
    // 重新获取列表数据并渲染页面
    if (res.data.status === 0) {
      // 更新列表数据并显示
      this.getCategorys()
    }
  }

  // 显示更新的对话框
  showUpdate = (category) => {
    // console.log(category);
    // 保存一下当前选中的这个category
    this.myCategory = category
    // console.log(this);
    console.log('myCategory@@', this.myCategory);
    this.setState({
      isModalVisible: 2
    })
  }
  // 更新商品分类
  updateCategory = async () => {
    // 隐藏对话框
    this.setState({
      isModalVisible: 0
    })
    // 修改后新的数据
    // id还是这个id
    // 修改后的名字需要从子组件的input框中获取
    const categoryId = this.myCategory._id
    const categoryName = this.newCategoryName

    console.log('数据库已更新', { categoryId, categoryName });

    // 发请求
    const res = await reqUpdateCategorys({ categoryId, categoryName })
    // 重新显示列表
    // console.log(res);
    if (res.data.status === 0) {
      // 更新列表数据并显示
      this.getCategorys()
    }
  }

  getNewCategoryName = (name) => {
    // 将这个函数作为props传给子组件
    // 子组件在合适的时候调用这个函数，将数据放在形参的位置
    // 父组件拿到数据，保存给this.newCategoryName
    this.newCategoryName = name
    // console.log('新名字@@', this.newCategoryName);
  }

  getNewSelect = (parentId) => {
    this.newSelect = parentId
    console.log('select被调用了', parentId);
  }

  componentWillMount() {
    this.initColumns()
  }

  componentDidMount() {
    this.getCategorys()
  }
  render() {

    // 读取状态数据
    const {
      categorys,
      isLoading,
      parentId,
      subCategorys,
      parentName,
      isModalVisible,
    } = this.state
    const title = parentId === '0' ? '一级分类标题' : (
      <span>
        {/* <LinkButton onClick={() => {this.showParent()}}>一级分类标题</LinkButton> */}
        <LinkButton onClick={this.showParent}>一级分类标题</LinkButton>
        <ArrowRightOutlined style={{ margin: '0px 20px' }} />
        {parentName}
      </span>
    )
    const extra = (
      <Button type='primary' onClick={this.showAdd}>
        <PlusOutlined />
        添加
      </Button>
    )
    const myCategory = this.myCategory || {}
    return (
      <Card title={title} extra={extra} bordered={false}>
        <Table
          dataSource={parentId === '0' ? categorys : subCategorys}
          columns={this.columns}
          bordered
          rowKey='_id'
          pagination={{ defaultPageSize: 6, showQuickJumper: true }}
          loading={isLoading}
        />

        <Modal
          title="添加分类列表"
          visible={isModalVisible === 1}
          onOk={this.addCategory}
          onCancel={this.handleCancel}
        >
          <AddForm
            categorys={categorys}
            parentId={parentId}
            getNewCategoryName={this.getNewCategoryName}
            getNewSelect={this.getNewSelect}
          />
        </Modal>

        <Modal
          title="更新分类列表"
          visible={isModalVisible === 2}
          onOk={this.updateCategory}
          onCancel={this.handleCancel}
        >
          <UpdateForm
            initCategoryName={myCategory.name}
            getNewCategoryName={this.getNewCategoryName}
          />
        </Modal>
      </Card>
    )
  }
}