import React, { Component } from 'react';
import { Form, Input, Button, Checkbox, message } from 'antd';
import { UserOutlined, LockOutlined } from '@ant-design/icons';
import './login.css';
import logoPic from '../../assets/images/logo192.png'
import {reqLogin} from '../../api'
import userInfo from '../../utils/memoryUtils'
import storageUtils from '../../utils/storageUtils'
import { Redirect } from 'react-router';
/**
 * 登陆路由组件
 */
export default class Login extends Component {
    render() {
        // 如果用户已经登录，自动跳转到管理界面
        // 即使在地址栏输入login的url也会强制跳转到管理界面
        const user = userInfo.user
        if(user && user._id){
            return <Redirect to='/'/>
        }
        const onFinish = (values) => {
            // console.log('Received values of form: ', values);
            const {username,password} = values
            reqLogin(username,password)
            .then(response => {
                console.log(response);
                console.log(response.data);
                const result = response.data
                if(result.status === 0){
                    // 登录成功
                    message.success('您已成功登录~')
                    // 保存用户登录信息
                    // console.log('@@',result);
                    const user = result.data
                    userInfo.user = user
                    storageUtils.saveUser(user)
                    // 不需要回退，所以使用replace方法
                    this.props.history.replace('/')
                }
                else{
                    // 登录失败
                    message.error(result.msg)
                }
            })
            .catch(error =>{
                console.log('失败了！',error);
            })
            
            // 使用async await 简化
            // const response = await reqLogin(username,password)
        }
        return (
            <div className='login'>
                <header className='login-header'>
                    <img src={logoPic} alt="logo" />
                    <h1>井后台</h1>
                </header>
                <section className='login-content'>
                    <h2>用户登录</h2>
                    <Form
                        name="normal_login"
                        className="login-form"
                        initialValues={{
                            remember: true,
                        }}
                        onFinish={onFinish}
                    >
                        <Form.Item
                            name="username"
                            rules={[
                                // 声明式验证
                                {
                                    required: true,
                                    message: '给👴输入宁的用户名',
                                },
                                {
                                    min: 4,
                                    message: '用户名至少四位！',
                                },
                                {
                                    max: 16,
                                    message: '用户名最多十六位！',
                                },
                                // {
                                //     pattern: /^[a-z0-9_]+$/ig,
                                //     message:'用户名只能由字母、数字和下划线构成'
                                // },
                            ]}
                        >
                            <Input prefix={<UserOutlined className="site-form-item-icon" style={{ color: 'rgba(0,0,0,0.5)' }} />}
                                placeholder="用户名"
                            />
                        </Form.Item>
                        <Form.Item
                            name="password"
                            rules={[
                                {
                                    required: true,
                                    message: '密码也别忘了输入咯！',
                                },
                            ]}
                        >
                            <Input
                                prefix={<LockOutlined className="site-form-item-icon" style={{ color: 'rgba(0,0,0,0.5)' }} />}
                                type="password"
                                placeholder="密码"
                            />
                        </Form.Item>
                        <Form.Item>
                            <Form.Item name="remember" valuePropName="checked" noStyle>
                                <Checkbox>记住密码</Checkbox>
                            </Form.Item>

                            <a className="login-form-forgot" href="https://www.baidu.com">
                                忘记密码
                            </a>
                        </Form.Item>

                        <Form.Item>
                            <Button type="primary" htmlType="submit" className="login-form-button">
                                登录
                            </Button>
                            或者   <a href="https://www.baidu.com">现在注册～</a>
                        </Form.Item>
                    </Form>
                </section>
            </div>
        )
    }
}

/**
 * 1、前台表单验证
 * 2、提交按钮发送请求
 */


// 高阶组件
// 接收一个组件，返回新组建（包装组件）包装组件会向被包装组件传入特定属性