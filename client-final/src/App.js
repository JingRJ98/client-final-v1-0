/**
 * 根组件
 */

import React, { Component } from 'react'
import { BrowserRouter, Route, Switch } from 'react-router-dom'
import { Button, message } from 'antd'
import 'antd/dist/antd.css';

import Login from './pages/login/login'
import Admin from './pages/admin/admin'
export default class App extends Component {
  handleClick = () => {
    message.error('失败啦！！～～～')
  }
  render() {
    // return <div>App</div>
    // return <Button type='primary' onClick={this.handleClick}>点我</Button>
    return (
      <BrowserRouter>
        <Switch>
          {/* 只匹配其中一个 */}
          <Route path='/login' component={Login}></Route>
          <Route path='/' component={Admin}></Route>
        </Switch>
      </BrowserRouter>
    )

  }
}
