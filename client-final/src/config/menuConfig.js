import {
  PieChartOutlined,
  AreaChartOutlined,
  BarChartOutlined,
  NumberOutlined,
  ShopOutlined,
  GoldOutlined,
  HddOutlined,
  UserOutlined,
  TeamOutlined,
  LineChartOutlined,
  BarsOutlined,
} from '@ant-design/icons';
const menuList = [
  {
    title: '首页', // 菜单标题名称
    key: '/home', // 对应的path
    icon: <NumberOutlined />, // 图标名称
    isPublic: true, // 公开的
  },
  {
    title: '商品',
    key: '/products',
    icon: <ShopOutlined />,
    children: [ // 子菜单列表
      {
        title: '品类管理',
        key: '/category',
        icon: <GoldOutlined />,
      },
      {
        title: '商品管理',
        key: '/product',
        icon: <HddOutlined />,
      },
    ]
  },

  {
    title: '用户管理',
    key: '/user',
    icon: <UserOutlined />,
  },
  {
    title: '角色管理',
    key: '/role',
    icon: <TeamOutlined />,
  },

  {
    title: '图形图表',
    key: '/charts',
    icon: <LineChartOutlined />,
    children: [
      {
        title: '柱形图',
        key: '/charts/bar',
        icon: <BarChartOutlined />,
      },
      {
        title: '折线图',
        key: '/charts/line',
        icon: <AreaChartOutlined />,
      },
      {
        title: '饼图',
        key: '/charts/pie',
        icon: <PieChartOutlined />,
      },
    ]
  },

  // {
  //   title: '订单管理',
  //   key: '/order',
  //   icon: <BarsOutlined />,
  // },
]

export default menuList