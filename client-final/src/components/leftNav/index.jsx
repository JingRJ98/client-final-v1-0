import React, { Component } from 'react'
import menuList from '../../config/menuConfig'
import './index.css'
import logo from '../../assets/images/logo192.png'
import { Link ,withRouter} from 'react-router-dom'
import { Menu } from 'antd';
import { YahooFilled } from '@ant-design/icons'

/**
 * 左侧导航组件
 * 
 */
const { SubMenu } = Menu;
class LeftNav extends Component {

  getMenuNodes_map = (menuList) => {
    return menuList.map(item => {
      if (!item.children) {
        return (
          <Menu.Item key={item.key} icon={item.icon}>
            <Link to={item.key}>
              {item.title}
            </Link>
          </Menu.Item>
        )
      }
      else {
        return (
          <SubMenu key={item.key} icon={item.icon} title={item.title}>
            {/* 递归调用 */}
            {this.getMenuNodes(item.children)}
          </SubMenu>
        )
      }
    })
  }

  // 使用reduce实现
  getMenuNodes = (menuList) => {
    return menuList.reduce((pre, item) => {

      // 向pre中添加<Menu.Iten>  或者<SubMenu>
      if (!item.children) {
        pre.push((
          <Menu.Item key={item.key} icon={item.icon}>
            <Link to={item.key}>
              {item.title}
            </Link>
          </Menu.Item>
        ))
      }
      else {
        // 找到某个元素，这个元素是item的子元素，且key和当前请求路径的path相同
        // const cItem = item.children.find(cItem => cItem.key === path)
        // this.openKey = item.key
        pre.push((
          <SubMenu key={item.key} icon={item.icon} title={item.title}>
            {/* 递归调用 */}
            {this.getMenuNodes(item.children)}
          </SubMenu>
        ))
      }
      return pre
    }, [])
  }
  render() {
    // 得到当前请求的路径
    // const menuNodes = this.getMenuNodes(menuList)
    const path = this.props.location.pathname
    // const openKey = this.openKey
    return (
      <div className='left-nav'>
        <Link to='/' className='left-nav-header'>
          <img src={logo} alt="logo" />
          <h1>井 ʕ◉ᴥ◉ʔ</h1>
        </Link>

        {/* 菜单 */}
        <Menu
          // defaultSelectedKeys={[path]}
          selectedKeys={[path]}
          // defaultOpenKeys={['sub1']}
          mode="inline"
          theme="dark"
        // inlineCollapsed={this.state.collapsed}
        >
          {/* 不使用静态写死的结构 */}
          {/* ------------------------------------------------------------------------ */}


          {/* <Menu.Item key="1" icon={<MenuFoldOutlined />}>
            <Link to='/home'>
              首页
            </Link>
          </Menu.Item>
          <SubMenu key="sub1" icon={<MailOutlined />} title="商品">
            <Menu.Item key="2" icon={<AppstoreOutlined />}>
              <Link to='/category'>
                品类管理
              </Link>
            </Menu.Item>
            <Menu.Item key="3" icon={<AppstoreOutlined />}>
              <Link to='/product'>
                商品管理
              </Link>
            </Menu.Item>
          </SubMenu>
          <Menu.Item key="4" icon={<AppstoreOutlined />}>
            <Link to='/user'>
              用户管理
            </Link>
          </Menu.Item>
          <Menu.Item key="5" icon={<AppstoreOutlined />}>
            <Link to='/role'>
              角色管理
            </Link>
          </Menu.Item>
          <SubMenu key="sub2" icon={<MailOutlined />} title="图形图表">
            <Menu.Item key="6" icon={<AppstoreOutlined />}>
              <Link to='/charts/bar'>
                柱状图
              </Link>
            </Menu.Item>
            <Menu.Item key="7" icon={<AppstoreOutlined />}>
              <Link to='/charts/line'>
                折线图
              </Link>
            </Menu.Item>
            <Menu.Item key="8" icon={<AppstoreOutlined />}>
              <Link to='/charts/pie'>
                饼图
              </Link>
            </Menu.Item>
          </SubMenu>
        
         */}
          {/* ------------------------------------------------------------------------ */}

          {this.getMenuNodes(menuList)}
        </Menu>
      </div>
    )
  }
}

// 高阶组件，包装别的组件
// 包装非路由组件，返回一个新的组件
// 会向被包装的组件传递history location，match三个属性
export default withRouter (LeftNav)