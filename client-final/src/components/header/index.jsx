import React, { Component } from 'react'
import { withRouter } from 'react-router-dom'
import './index.css'
// import img from '../../assets/images/logo192.png'
import { reqWeather } from '../../api/index.js'
import menuList from '../../config/menuConfig'
import { formateDate } from '../../utils/dateUtils'
import userInfo from '../../utils/memoryUtils'
import storageUtils from '../../utils/storageUtils'
import { Modal } from 'antd'
import { ExclamationCircleOutlined } from '@ant-design/icons'
import LinkButton from '../linkButton'
import moment from 'moment'
/**
 * 头部组件
 * 
 */
class Header extends Component {
  state = {
    // 当前时间字符串
    currentTime: formateDate(Date.now()),
    city: '',
    province: '',
    weather: '',
    temperature: 0
  }

  getTime = () => {
    // 每隔一秒获取时间
    this.intervalId = setInterval(() => {
      // const currentTime = formateDate(Date.now())
      const currentTime = moment().format('YY/MM/DD HH:mm')

      this.setState({ currentTime })
    }, 1000)
  }

  getWeather = async () => {
    // 解构收到的data
    const { city, province, weather, temperature } = await reqWeather('320114')

    // 更新状态
    this.setState({ city, province, weather, temperature })
  }

  getTitle = () => {
    // 得到当前请求的路径
    const path = this.props.location.pathname
    let title
    menuList.forEach(item => {
      if (item.key === path) {
        title = item.title
      }
      else if (item.children) {
        const cItem = item.children.find(cItem => cItem.key === path)
        if (cItem) title = cItem.title

      }
    })
    return title
  }

  /**
   * 退出登录
   */
  logout = () => {
    // const self = this

    // 退出确认
    Modal.confirm({
      title: '确认退出吗？',
      icon: <ExclamationCircleOutlined />,
      okText: '确认',
      cancelText: '取消',
      onOk: () => {
        // console.log('hhh');
        // 删除保存的user数据
        storageUtils.removeUser()
        userInfo.user = {}
        // 跳转到登录页面
        this.props.history.replace('/login')
      },
    })
  }

  /**
   * 第一次render之后执行一次
   * 一般在此执行异步请求
   */
  componentDidMount() {
    this.getTime()
    this.getWeather()
  }
  /**
   * react组件已经被销毁，而此时我们的异步操作（通常为调用接口更改state操作）还未结束。
   * 当我们的异步执行完成后要执行setState操作时，已经无法获得组件信息，由此造成该异常
   * 我们应该在组件中通过componentWillUnmount钩子函数在组件销毁的时候将异步方法撤销：
   */
  componentWillUnmount() {
    // 清除定时器
    clearInterval(this.intervalId)
    this.setState = () => false;
  }
  render() {
    const { currentTime, city, province, weather, temperature } = this.state
    const userName = userInfo.user.username
    const title = this.getTitle()
    return (
      <div className='header'>
        <div className='header-top'>
          <span>欢迎，{userName}</span>
          <LinkButton onClick={this.logout}>退出</LinkButton>
          {/* <a href="javascript:;" onClick={this.logout}>退出</a> */}
        </div>

        <div className='header-bottom'>
          <div className="header-bottom-left">
            <span>{title}</span>
          </div>
          <div className="header-bottom-right">
            <span className="header-bottom-right-left">{province + ' ' + city}</span>
            <span className="header-bottom-right-left">{currentTime}</span>
            <span>{weather}</span>
          </div>
        </div>
      </div>
    )
  }
}

export default withRouter(Header)